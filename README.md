# README
These are the deliverables for Nautilus' recruitment process. Following this README you will find brief descriptions of all the repository's contents and instructions on how to use them. More explanations on how the codes work can be obtained by reading the comments on the individual codes.

## mult.py and fibonacci.py 
These python codes are targeted to solve project Euler's first and second problems, which can be found [here.](https://projecteuler.net)

### How to run it?
After cloning this repository, using the terminal, go to the correct folder and type `mult.py` or `fibonacci.py`

## championship.py


### How to run it?


## talker.py, listener.py and oddlistener.py
Talker.py creates a talking node that communicates to a listener node (listener.py) by the same topic (channel). The oddlistener.py has both the talker and the listener in the same script, and sends numbers that increase by 1 each time, but only displays the odd numbers recovered by the listener.

### How to run it?
For these programs you will need to install ROS. A quick guide can be found [here.](http://wiki.ros.org/melodic/Installation/Ubuntu) After that,  you'll need to [create a workspace](http://wiki.ros.org/catkin/Tutorials/create_a_workspace) and add these codes to the `scirpts` folder you will make.

## solar_system


### How to run it?


## model/grabber11


### How to run it?


## nemo


### How to run it?
